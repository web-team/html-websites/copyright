# Copyright

Website containing the Terms of Use for CERN Audiovisual Media (i.e. CDS).

Previously hosted as a Drupal website. Migrated to a static website 02-JUN-2023.